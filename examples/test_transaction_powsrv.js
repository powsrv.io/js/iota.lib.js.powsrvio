const remoteATT = require('../powsrv.js')
const attachToTangle = remoteATT(5000, "your9api9key")

const { composeAPI } = require('@iota/core')

const iota = composeAPI({
    provider: 'http://localhost:14265', 	// your node's address (no activated attachToTangle needed)
    attachToTangle				// use powsrv.io attachToTangle 
})

// Must be truly random & 81-trytes long.
const seed = 'PUKGTPIHCKALPIPEWP99IBLDV9VRZRPMQZECREPCXYGYYEHCMZCDUUGYIXNPMBHRYFL9OMN9AAPEKZQZW' // dummy seed, do not use it for value transfers!

// Array of transfers which defines transfer recipients and value transferred in IOTAs.
const transfers = [{
    address: '999999999999999999999999999999999999999999999999999999999999999999999999999999999',
    value: 0,
    tag: 'POWSRV9JS9LIB9TEST',
    message: 'WBMDEAUCXCFDGDHDEAHDFDPCBDGDPCRCHDXCCDBDEAKDXCHDWCEAUCPCGDHDEAZBCDFCEASCCDBDTCEAQCMDEADDCDKDGDFDJDSAXCCD'
}]

// Depth or how far to go for tip selection entry point.
const depth = 3 

// Difficulty of Proof-of-Work required to attach transaction to tangle.
// Minimum value on mainnet is `14`, `7` on spamnet and `9` on devnet and other testnets.
const minWeightMagnitude = 14

// Prepare a bundle.
iota.prepareTransfers(seed, transfers)
    .then(trytes => {
        // Persist trytes locally before sending to network.
        // This allows for reattachments and prevents key reuse if trytes can't
        // be recovered by querying the network after broadcasting.

        // Does tip selection, attaches to tangle by doing PoW and broadcasts.
        return iota.sendTrytes(trytes, depth, minWeightMagnitude)
    })
    .then(bundle => {
        console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
        console.log(`Bundle: ${bundle}`)
    })
    .catch(err => {
        console.log(err)
    })
