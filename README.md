# iota.lib.js.powsrvio - Use powsrv.io PoW with IOTA.js Library

This library is used to add an alternative attachToTangle function to the [iota.js](https://github.com/iotaledger/iota.js) library to delegate Proof-of-work to [powsrv.io](https://powsrv.io). This enables for users to offload PoW from low-power IoT devices, mobile and desktop devices without capabilities.

## Getting Started

Add the package to your project:

```shell
npm install powsrv.js
```

## Basic Usage

```javascript
const remoteATT = require('powsrv.js')
const attachToTangle = remoteATT(5000, "your9api9key")

// Set attachToTangle to your IOTA client
const { composeAPI } = require('@iota/core')
const iota = composeAPI({
  provider: 'http://localhost:14265',
  attachToTangle
})
```

1. **delay**: `Integer` Milliseconds to wait before PoW server timeout. _Null value defaults to 5000_
2. **key**: `String` API key used to get access to powsrv.io.

## API Key

Register on [my.powsrv.io](https://my.powsrv.io) to get an API key

## My first transaction with PoW done by powsrv.io

1. Change to a directory of your choice.
2. Clone or download iota.lib.js.powsrvio
3. Change to the iota.lib.js.powsrvio/examples directory.
4. Install the iota.js core lib with `npm install @iota/core`.
5. Change the `provider` in `test_transaction_powsrv.js` to your node's address.
6. Change `your9api9key` to your API key
7. Run `node test_transaction_powsrv.js` (if there is no node.js on your system [install](https://nodejs.org/en/download/package-manager/) it first)
8. The output shuold be like this: `Published transaction with tail hash: XNQY...`
9. Great, now you are able to send IOTA transactions with super fast PoW. 

## My first public MAM stream with PoW done by powsrv.io

1.  Change to a directory of your choice.
2.  Clone or download iota.lib.js.powsrvio
3.  Change to the iota.lib.js.powsrvio/examples directory.
4.  Install the mam.client.js lib with `npm install @iota/mam`.
5.  Install the iota.js core lib with `npm install @iota/core`.
6.  Change the `provider` in `publishAndFetchPublic.js` to your node's address.
7.  Change `your9api9key` to your API key
8.  Run `node publishAndFetchPublic.js` (if there is no node.js on your system [install](https://nodejs.org/en/download/package-manager/) it first)
9.  The output shuold be like this: `Published { message: 'Message from Alice',timestamp: '2019-2-10 10:54:47' } ...`
10. Great, now you are able to publish MAM streams with super fast PoW

